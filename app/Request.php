<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    //
    protected $table = 'requests';
    public $timestamps = false;

    public function operator()
    {
        return $this->belongsTo('App\Operator','operator_id','id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Offer','offer_id','id');
    }
}
