<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Request as SqRequest;
use App\Operator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller
{
    //

    public function index()
    {
        $data = [
            'title' => 'Home'
        ];

        return view(config('settings.theme').'.site.index',$data);
    }

    public function first()
    {
        $requests = SqRequest::where('count','>','2')->whereIn('operator_id', [10, 12])->orderBy('id')->get();

        //$requests = DB::select('SELECT requests.id,price,requests.count,offers.name AS offer_name,operators.name AS operator_name FROM requests LEFT JOIN offers ON requests.offer_id = offers.id LEFT JOIN operators ON requests.operator_id = operators.id WHERE count > ? AND operator_id IN (?,?)',[2,10,12]);

        $data = [
            'requests' => $requests,
            'title' => 'First request'
        ];

        return view(config('settings.theme').'.site.first',$data);
    }

    public function second()
    {
        $requests = SqRequest::groupBy('offer_id')->select(DB::raw('sum(count) AS count,sum(price) AS price, offer_id'))->get();

        //$requests = DB::select('SELECT offers.name AS offer_name,SUM(price) AS price, SUM(requests.count) AS product_count FROM requests LEFT JOIN offers ON requests.offer_id = offers.id GROUP BY offer_id');

        $data = [
            'requests' => $requests,
            'title' => 'Second request'
        ];

        return view(config('settings.theme').'.site.second',$data);
    }

    public function cart(Request $request)
    {
        $id = $request->id;

        $order = SqRequest::where('id',$id)->first();

        echo "Номер заказа: {$order->id}, Имя товара: {$order->offer->name}, Цена: {$order->price}, Количество: {$order->count}, Имя оператора: {$order->operator->name}";
    }
}
