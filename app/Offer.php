<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    //
    protected $table = 'offers';
    public $timestamps = false;

    public function requests()
    {
        return $this->hasMany('App\Request','offer_id','id');
    }
}
