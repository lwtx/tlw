<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    //
    protected $table = 'operators';
    public $timestamps = false;

    public function requests()
    {
        return $this->hasMany('App\Request','operator_id','id');
    }
}
