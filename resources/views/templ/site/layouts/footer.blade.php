<div class="container-fluid footer">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <p class="footer-copy"><?= Carbon\Carbon::createFromDate()->format('Y') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>