<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset(config('settings.theme')) }}/site/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset(config('settings.theme')) }}/site/css/style.css">

    <title>{{ isset($title) ? $title.'-'.config('settings.appName') : config('settings.appName') }}</title>
</head>
<body>

@include(config('settings.theme').'.site.layouts.header')

@hasSection('content')
    <div class="container section-content">
        <div class="row">
            <div class="col">
                @yield('content')
            </div>
        </div>
    </div>
@endif

@include(config('settings.theme').'.site.layouts.footer')

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="{{ asset(config('settings.theme')) }}/site/js/bootstrap.min.js"></script>
<script src="{{ asset(config('settings.theme')) }}/site/js/main.js"></script>
</body>
</html>