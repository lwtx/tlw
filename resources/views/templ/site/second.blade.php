@extends(config('settings.theme').'.site.layouts.layout')

@section('content')
    @isset($requests)
        <a href="#" class="btn btn-danger button-hide">Скрыть</a>
        <a href="#" class="btn btn-success button-show">Показать</a>
        <div class="table-responsive section-table">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Имя товара</th>
                    <th>Количество товара</th>
                    <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                @foreach($requests as $request)
                    <tr>
                        <td>
                            {{ $request->offer->name }}
                            {{--{{ $request->offer_name }}--}}
                        </td>
                        <td>
                            {{ $request->count }}
                            {{--{{ $request->product_count }}--}}
                        </td>
                        <td>
                            {{ $request->price }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endisset
@endsection