@extends(config('settings.theme').'.site.layouts.layout')

@section('content')
    @isset($requests)
        <a href="#" class="btn btn-danger button-hide">Скрыть</a>
        <a href="#" class="btn btn-success button-show">Показать</a>
        <div class="table-responsive section-table">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Номер заказа</th>
                    <th>Имя товара</th>
                    <th>Цена</th>
                    <th>Количество</th>
                    <th>Имя оператора</th>
                    <th>Добавить</th>
                </tr>
                </thead>
                <tbody>
                @foreach($requests as $request)
                    <tr>
                        <td>
                            {{ $request->id }}
                        </td>
                        <td>
                            {{ $request->offer->name }}
                            {{--{{ $request->offer_name}}--}}
                        </td>
                        <td>
                            {{ $request->price }}
                        </td>
                        <td>
                            {{ $request->count }}
                        </td>
                        <td>
                            {{ $request->operator->name }}
                            {{--{{ $request->operator_name }}--}}
                        </td>
                        <td>
                            <a href="#" data-id="{{ $request->id }}" class="btn btn-primary add-to-cart">Добавить</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endisset
@endsection