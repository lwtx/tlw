$(document).ready(function(){
    $(".button-hide").on('click', function (e){
        e.preventDefault();
        $(".section-table").hide();
    });

    $(".button-show").on('click', function (e){
        e.preventDefault();
        $(".section-table").show();
    });

    $(".section-table").mouseover(function(){
        $(".section-table th").css({"color": "green"});
    });

    $(".section-table").mouseout(function(){
        $(".section-table th").css({"color": "black"});
    });


    $('.add-to-cart').click(function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        $.ajax(
            {
                url:'/cart',
                type:'get',
                data:{id:id},
                success:function (result) {
                    alert("Вы добавили заказ - " + result)
                },
                error: function () {
                    alert("Fault");
                },

                dataType:"text",
            }
        );
    });

});